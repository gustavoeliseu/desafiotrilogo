package com.gustavo.desafiotrilogo.viewmodel

import android.graphics.drawable.Drawable
import androidx.lifecycle.ViewModel
import com.gustavo.desafiotrilogo.models.MyMovie

class MovieViewModel(movie:MyMovie):ViewModel() {
    val id = movie.id
    val title = movie.title
    val poster_path = movie.poster_path
}