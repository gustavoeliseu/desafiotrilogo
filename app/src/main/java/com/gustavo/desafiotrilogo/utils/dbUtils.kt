package com.gustavo.desafiotrilogo.utils

//Embora alguns não sejam utilizados, foram criados para permitir uma melhor seleção
//movie list link
//Exemplo de querry: https://api.themoviedb.org/3/movie/now_playing?api_key=c2e78b4a8c14e65dd6e27504e6df95ad&language=pt-BR&page=1&region=BR
const val api_key = "c2e78b4a8c14e65dd6e27504e6df95ad"
const val movieDBHtml = "https://api.themoviedb.org/3/movie/now_playing?api_key="
const val urlLanguage = "&language="
const val pageUrlQuerry = "&page="
const val moviesRegion = "&region="

const val averageIMDbRating = 6.8

//Movie List Image url
const val baseImageDownloadURL="https://image.tmdb.org/t/p/w500/"

