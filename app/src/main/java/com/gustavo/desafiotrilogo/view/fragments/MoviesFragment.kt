package com.gustavo.desafiotrilogo.view.fragments

import android.content.Context
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.gustavo.androidjya.adapters.MoviesRecyclerViewAdapter
import com.gustavo.desafiotrilogo.R
import com.gustavo.desafiotrilogo.view.activities.MainActivity
import com.gustavo.desafiotrilogo.models.MyMovie
import com.gustavo.desafiotrilogo.network.HttpGetRequest
import com.gustavo.desafiotrilogo.utils.*
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Fragment responsible for representing the list of movies from theMovieDB
 *
 * By Gustavo Amaral Eliseu 03/02/2020
 */
class MoviesFragment : Fragment() {

    private var columnCount = 2
    var movieList: ArrayList<MyMovie> = ArrayList()

    private lateinit var myRecyclerAdapter:MoviesRecyclerViewAdapter

    private var listener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getDataFromNetwork(1)
        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    fun getDataFromNetwork(page:Int){

        //Mounting the query based on user's android's default location
        val getJsonClass = HttpGetRequest().execute(movieDBHtml + api_key + urlLanguage + Locale.getDefault().language+ pageUrlQuerry +page+ moviesRegion + Locale.getDefault().country)

        val gson = Gson()
        var jsonArray = JSONObject(getJsonClass.get()).getJSONArray("results")
        //Getting the MovieList from the query
        for (jsonCounter in 0 until jsonArray.length()) {
            val movie = gson.fromJson(jsonArray.getString(jsonCounter), MyMovie::class.java)
            movie.release_date = localizeDate(movie.release_date)
            if (movie.overview.isNullOrEmpty()) {
                movie.overview = getString(R.string.no_overview)
            }
            movieList.add(movie)
        }

    }

    //Set the Release date based on the default localization of the Android (ex: en = mm/dd/yy)
    fun localizeDate(dateString: String): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        var date: Date? = null
        try {
            date = sdf.parse(dateString)
        } catch (e: ParseException) {
            Log.e("DateParseErr: ", e.toString())
        }
        val dateFormat = DateFormat.getDateFormat(context)
        return dateFormat.format(date)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_movies_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {

            with(view) {

                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                myRecyclerAdapter=
                    MoviesRecyclerViewAdapter(
                        movieList,
                        listener,
                        context
                    )
                adapter=myRecyclerAdapter

            }

        }
        return view
    }

    override fun onStart() {
        super.onStart()
        myRecyclerAdapter.notifyDataSetChanged()

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.setTitle(getString(R.string.movies_session))
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: MyMovie?, imageView: ImageView)
    }

    companion object {
        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(columnCount: Int) =
            MoviesFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}
