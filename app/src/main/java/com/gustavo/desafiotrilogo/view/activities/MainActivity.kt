package com.gustavo.desafiotrilogo.view.activities

import android.os.Build
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN
import com.google.gson.Gson
import com.gustavo.desafiotrilogo.R
import com.gustavo.desafiotrilogo.databinding.ActivityMainBinding
import com.gustavo.desafiotrilogo.databinding.FragmentSelectedMovieBinding
import com.gustavo.desafiotrilogo.models.MyMovie
import com.gustavo.desafiotrilogo.network.HttpGetRequest
import com.gustavo.desafiotrilogo.utils.*
import com.gustavo.desafiotrilogo.view.fragments.MoviesFragment
import com.gustavo.desafiotrilogo.view.fragments.SelectedMovieFragment
import io.reactivex.processors.PublishProcessor
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Gustavo Amaral Eliseu 03/02/2020
 *
 * Nichollas, como eu havia informado ao Hugo, estou com problemas com a NetVirtua que só será resolvido amanhã.
 * Por isso precisei simplificar bastante o código para que eu pudesse testar o mesmo antes de enviar.
 *
 * Caso ainda seja possível, amanhã irei fazer uma versão bem aprimorada do mesmo com um melhor acesso ao catálogo,
 * uma segunda tela(detalhes do filme), animação de transição, favoritos, paginação etc...
 * Também irei fazer uma proteção levando em conta os possíveis respostas oriundas do MoviesDB (200, 401 e 404).
 * */


//TODO - Melhorar acessibilidade da aplicação


class MainActivity : AppCompatActivity(), MoviesFragment.OnListFragmentInteractionListener {
    val manager = supportFragmentManager
    private val paginator: PublishProcessor<Integer> = PublishProcessor.create()
    //TagsFragments
    val TAGMoviesListFragment: String = "MoviesListFragment"
    val TAGMovieSelectedFragment: String = "MovieSelectedFragment"

    val MoviesListFragment: MoviesFragment = MoviesFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
    }

    override fun onStart() {
        super.onStart()



        changeFragment(MoviesListFragment, TAGMoviesListFragment)
    }






    //controle do back button
    override fun onBackPressed() {
        val selectedFrag:SelectedMovieFragment? = manager.findFragmentByTag(TAGMovieSelectedFragment) as SelectedMovieFragment?
        if (selectedFrag==null || selectedFrag.isVisible.not() ) {
            finish()
        } else {
            changeFragment(MoviesListFragment, TAGMoviesListFragment)
        }


    }

    fun changeFragment(fragment: Fragment, TAG: String) {
        val transaction = manager.beginTransaction()
        if (manager.fragments.isEmpty()) {
            transaction.add(R.id.mainFrameLayout, fragment, TAG)
        } else {
            transaction.replace(R.id.mainFrameLayout, fragment, TAG)
        }
        transaction.addToBackStack(null)
        transaction.commit()
        manager.executePendingTransactions()


    }

    //OnClick do fragmento Lista
    override fun onListFragmentInteraction(item: MyMovie?, imageView: ImageView) {
        if (item != null) {
            val selectedMovieFragment = SelectedMovieFragment(item)
            changeFragment(selectedMovieFragment, TAGMovieSelectedFragment)
        }
    }


}
