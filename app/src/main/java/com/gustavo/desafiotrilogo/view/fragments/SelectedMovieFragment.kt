package com.gustavo.desafiotrilogo.view.fragments

import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.gustavo.desafiotrilogo.R
import com.gustavo.desafiotrilogo.databinding.FragmentSelectedMovieBinding
import com.gustavo.desafiotrilogo.models.MyMovie
import com.gustavo.desafiotrilogo.utils.averageIMDbRating
import com.gustavo.desafiotrilogo.utils.baseImageDownloadURL
import com.gustavo.desafiotrilogo.view.activities.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Gustavo Amaral Eliseu 07/02/2020
 *
 * Fragment
 * */
class SelectedMovieFragment(val myMovie:MyMovie) : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentSelectedMovieBinding.inflate(this.layoutInflater, mainFrameLayout, false)
        val imgDownloadUrl = baseImageDownloadURL+myMovie.poster_path
        binding.selectedRequestPullDrawable.load(imgDownloadUrl)

        //Setting the color for the voteAverageText
        if(myMovie.vote_count>0&& context!= null){
            if(myMovie.vote_average.toFloat()> averageIMDbRating){
                binding.textAvg.setTextColor((context as MainActivity).resources.getColor(R.color.textGreen))
            } else {
                binding.textAvg.setTextColor((context as MainActivity).resources.getColor(R.color.textRed))
            }
        }else{
            myMovie.vote_average = "-/-"
            binding.textAvg.setTextColor((context as MainActivity).resources.getColor(R.color.textYellow))

        }
        binding.setMyMovie(myMovie)
        binding.executePendingBindings()

        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance(myMovie:MyMovie) =
            SelectedMovieFragment(myMovie).apply {
                arguments = Bundle().apply {
                }
            }
    }

    //Downloading and loading the image with Glide
    fun ImageView.load(downloadUri: String, loadOnlyFromCache: Boolean = false) {
        val requestOptions = RequestOptions.placeholderOf(R.drawable.poster_placeholder)
            .dontTransform()
            .onlyRetrieveFromCache(loadOnlyFromCache)
        Glide.with(this)
            .load(downloadUri)
            .into(this);
    }
}

