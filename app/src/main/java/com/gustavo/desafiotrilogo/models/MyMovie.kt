package com.gustavo.desafiotrilogo.models

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.graphics.drawable.toDrawable

class MyMovie (val title: String, var overview:String,val vote_count:Integer, val poster_path:String, var vote_average : String, val id: String, var release_date:String, var poster:Drawable){


}
