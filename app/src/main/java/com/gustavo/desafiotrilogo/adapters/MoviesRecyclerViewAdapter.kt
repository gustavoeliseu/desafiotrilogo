package com.gustavo.androidjya.adapters


import android.content.Context
import android.graphics.drawable.Drawable
import android.media.Image
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.gustavo.desafiotrilogo.R
import com.gustavo.desafiotrilogo.databinding.ItemMovieBinding
import com.gustavo.desafiotrilogo.models.MyMovie
import com.gustavo.desafiotrilogo.utils.baseImageDownloadURL
import com.gustavo.desafiotrilogo.view.activities.MainActivity
import com.gustavo.desafiotrilogo.view.fragments.MoviesFragment
import com.gustavo.desafiotrilogo.viewmodel.MovieViewModel
import kotlinx.android.synthetic.main.item_movie.view.*


/**
 * Adapter da lista de filmes
 *Created by Gustavo Amaral Eliseu
 * 03/02/2020
 */

//TODO - Configurar paginação para a lista
class MoviesRecyclerViewAdapter(
    private val mValues: List<MyMovie>,
    private val mListener: MoviesFragment.OnListFragmentInteractionListener?,
    private val context: Context
) : RecyclerView.Adapter<MoviesRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item =
                mValues.find { movie: MyMovie -> movie.id.equals((v.tag as MovieViewModel).id) }

            if (item != null)
                mListener?.onListFragmentInteraction(item,v.requestPullDrawable)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
        val itemMovieBinding = ItemMovieBinding.inflate(view, parent, false)

        return ViewHolder(itemMovieBinding.root, itemMovieBinding)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item: MovieViewModel = MovieViewModel(mValues[position])
        holder.bind(item)

        //Setting title as image description for accessibility
        holder.requestPullDrawable.contentDescription = item.title

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View, var binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(mView) {
        val requestPullDrawable: ImageView = mView.requestPullDrawable


        init {
            this.binding = binding;
        }

        override fun toString(): String {
            return super.toString() + " '"
        }

        //Downloading and loading the image with Glide
        fun ImageView.load(downloadUri: String) {
            Glide.with(this)
                .load(downloadUri)
                .into(this);
        }


        fun bind(movie: MovieViewModel?) {
            if (movie != null) {
                binding.setMovieViewModel(movie)
                requestPullDrawable.apply {
                    val imgDownloadUrl = baseImageDownloadURL + movie?.poster_path
                    requestPullDrawable.load(imgDownloadUrl)
                }
            }
            binding.executePendingBindings()
        }
    }

}
