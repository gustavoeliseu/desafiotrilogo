package com.gustavo.desafiotrilogo.network

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.text.format.DateFormat
import android.util.Log
import com.gustavo.desafiotrilogo.utils.api_key
import com.gustavo.desafiotrilogo.utils.movieDBHtml
import com.gustavo.desafiotrilogo.utils.urlLanguage
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.InputStream
import java.net.URL
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class HttpGetRequest(): AsyncTask<String, Void, String>() {
var client = OkHttpClient()


    override fun doInBackground(vararg p0: String?): String {
        val request: Request = Request.Builder()
            .url(p0[0])
            .build()
        val response = client.newCall(request).execute()

        return response.body()!!.string()
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)

    }



}
