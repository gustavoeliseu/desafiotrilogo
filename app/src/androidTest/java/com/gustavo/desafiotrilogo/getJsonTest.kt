package com.gustavo.desafiotrilogo

import android.util.Log
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.google.gson.Gson
import com.gustavo.desafiotrilogo.models.MyMovie
import com.gustavo.desafiotrilogo.network.HttpGetRequest
import com.gustavo.desafiotrilogo.utils.api_key
import com.gustavo.desafiotrilogo.utils.movieDBHtml
import com.gustavo.desafiotrilogo.utils.moviesPT
import org.json.JSONObject
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class getJsonTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        Assert.assertEquals("com.gustavo.desafiotrilogo", appContext.packageName)
    }

    @Test
    fun testGetJsonURL(){
        val getJsonClass = HttpGetRequest().execute(movieDBHtml + api_key + moviesPT)
        val gson = Gson()
        val movieList = ArrayList<MyMovie>()
        var jsonArray = JSONObject(getJsonClass.get()).getJSONArray("results")
        for(jsonCounter in 0 until jsonArray.length()){
            val movie = gson.fromJson(jsonArray.getString(jsonCounter) , MyMovie::class.java)
            movieList.add(movie)
        }

        for(x in 0 until movieList.size){
            Log.v("jsonTestTitle",if(movieList.get(x).title!=null)movieList.get(x).title else "null")
            Log.v("jsonTestVoteCount",if(movieList.get(x).vote_count!=null)movieList.get(x).vote_count.toString() else "null")
            Log.v("jsonTestPosterPath",if(movieList.get(x).poster_path!=null)movieList.get(x).poster_path else "null")
            Log.v("jsonTestVoteAverage",if(movieList.get(x).vote_average!=null)movieList.get(x).vote_average.toString() else "null")
        }
    }
}