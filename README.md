# DesafioTrilogo

Desafio para receber a lista de filmes em cartaz utilizando a api https://developers.themoviedb.org/3/movies/get-now-playing
e exibir titulo, cartaz, nota. Descrição do filme é exibida ao ser clicada. Resolvi simplificar o app, deixando apenas duas telas e demonstrando o básico do que foi requisitado.

Screenshots do aplicativo estão na pasta screenshots, na pasta raiz do projeto